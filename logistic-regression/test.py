from sklearn import datasets, model_selection
from sklearn import linear_model
from logistic import LogisticRegression

# 加载数据集
cancer = datasets.load_breast_cancer()
x = cancer.data  # 样本
y = cancer.target  # 类别

# 划分数据集
x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2)

lr = linear_model.LogisticRegression(penalty='l2', solver='newton-cg', multi_class='multinomial')
lr.fit(x_train, y_train)

print("逻辑回归模型训练集的准确率：%.3f" % lr.score(x_train, y_train))
print("逻辑回归模型测试集的准确率：%.3f" % lr.score(x_test, y_test))

my_lr = LogisticRegression()

my_lr.fit(x_train, y_train)
print("手写逻辑回归模型测试集的准确率：%.3f" % my_lr.score(x_test, y_test))
