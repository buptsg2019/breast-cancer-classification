from sklearn import datasets, model_selection
from sklearn import svm
from svm import SVM
# 加载数据集
cancer = datasets.load_breast_cancer()
x = cancer.data  # 样本
y = cancer.target  # 类别

# 划分数据集
x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2)

svm = svm.SVC(gamma='auto')
svm.fit(x_train, y_train)

print("SVM模型训练集的准确率：%.3f" % svm.score(x_train, y_train))
print("SVM模型测试集的准确率：%.3f" % svm.score(x_test, y_test))

my_svm = SVM(C=0.10)
my_svm.fit(x_train, y_train)
print("手写SVM模型测试集的准确率：%.3f" % my_svm.score(x_train, y_train))
