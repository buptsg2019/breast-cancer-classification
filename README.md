## breast-cancer-classification

### 简介

乳腺癌数据集分类，用多种机器学习方法实现，与 sklearn 作对比

### 进度

- [ ] K-近邻
- [ ] 朴素贝叶斯
- [ ] 逻辑回归
- [ ] 决策树
- [ ] 支持向量机

