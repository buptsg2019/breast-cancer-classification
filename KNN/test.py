from sklearn import datasets, model_selection
from sklearn import neighbors
from knn import KNN

# 加载数据集
cancer = datasets.load_breast_cancer()
x = cancer.data  # 样本
y = cancer.target  # 类别

# 划分数据集
x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2)

knn = neighbors.KNeighborsClassifier(n_neighbors=5)
knn.fit(x_train, y_train)

print("knn模型训练集的准确率：%.3f" % knn.score(x_train, y_train))
print("knn模型测试集的准确率：%.3f" % knn.score(x_test, y_test))

my_knn = KNN(x_train, y_train)
print("手写knn模型测试集的准确率：%.3f" % my_knn.score(x_train, y_train))
