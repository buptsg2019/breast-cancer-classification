## 乳腺癌数据集分类

### K-近邻算法

#### 算法实现

算法实现在 `knn.py` 中实现，将其封装为 `KNN` 类：

- 根据邻近点个数和距离（范数）类型进行初始化

```python
    def __init__(self, x_train, y_train, n_neighbors=3, p=2):  # 初始化数据，neighbor表示邻近点，p为欧氏距离
        self.n = n_neighbors
        self.p = p
        self.x_train = x_train
        self.y_train = y_train
```

- 因为 KNN 算法不需要训练，可以直接对测试集进行测试：

```python
    def predict(self, X):
        # X为测试集
        knn_list = []
        # 先遍历指定个邻近点，求范数
        for i in range(self.n):
            # 计算训练集和测试集之间的距离,np.linalg.norm求范数
            dist = np.linalg.norm(X - self.x_train[i], ord=self.p)
            knn_list.append((dist, self.y_train[i]))  # 在列表末尾添加一个元素

        # 对于剩下的数据集，求范数，并替换近邻中最大的点
        for i in range(self.n, len(self.x_train)):  # 3-20
            max_index = knn_list.index(max(knn_list, key=lambda x: x[0]))  # 找出列表中距离最大的点
            dist = np.linalg.norm(X - self.x_train[i], ord=self.p)  # 计算训练集和测试集之间的距离
            if knn_list[max_index][0] > dist:  # 若当前数据的距离大于之前得出的距离，就将数值替换
                knn_list[max_index] = (dist, self.y_train[i])

        # 把近邻点中标签提取出
        knn = [k[-1] for k in knn_list]
        # 统计标签的个数,Counter计算数组中每个元素出现的次数
        count_pairs = Counter(knn)
        max_count = sorted(count_pairs, key=lambda x: x)[-1]  # 将标签升序排列
        return max_count
```

- 批量预测测试集，并计算评分：

```python
    # 计算测试算法的正确率
    def score(self, x_test, y_test):
        right_count = 0
        n = 10
        for X, y in zip(x_test, y_test):
            label = self.predict(X)
            if label == y:
                right_count += 1
        return right_count / len(x_test)
```

#### 结果测试

sklearn 库函数与手写函数对比结果如下：

![image-20211031230559655](https://gitee.com/buptsg2019/picgo/raw/master/image-20211031230559655.png)

可以看到模型准确率均在 90% 以上，与 sklearn 库函数相差不大。

### SVM 支持向量机算法

#### 算法实现

算法实现在 `svm.py` 中，封装在 `SVM` 类里，主要说明一下训练函数和预测函数：

- 预测函数使用 Hinge 损失，并加上正则化项，最小化该损失函数。Hinge 函数为：
  $$
  L(z) = \max(0,1-z)
  $$
  由于 SVM 损失既需要考虑经验风险，也需要考虑结构风险，故最终损失函数为：
  $$
  Loss = \sum^N_{i=1}\max(0,1-y_i(\omega^{T}x+b))+\lambda||\omega||^{2}
  $$
  代码实现如下：

```python
    def fit(self, X, y, lr=1e-3, epochs=500):
        self.n, self.d = X.shape[0], X.shape[1]
        self.W = np.random.rand(self.d)
        self.b = np.random.rand()

        self.x = X
        self.y = y
        losses = []
        for i in range(epochs):
            margin = self.__margin(X, y)
            loss = self.__cost(margin)
            losses.append(loss)

            missclassified_pts_idx = np.where(margin < 1)[0]
            d_W = self.W - self.C * y[missclassified_pts_idx].dot(X[missclassified_pts_idx])
            self.W = self.W - lr * d_W

            d_gama = -self.C * np.sum(y[missclassified_pts_idx])
            self.b = self.b - lr * d_gama

            self._support_vectors = np.where(self.__margin(X, y) < 1)[0]
```

其中，正则化函数和损失函数为：

```python
    def __margin(self, X, y):
        return y * self.__desicion_function(X)

    def __cost(self, margin):
        return (1 / 2) * self.W.dot(self.W) + self.C * np.sum(np.maximum(0, 1 - margin))
```

#### 结果测试

sklearn 库函数与手写函数对比结果如下：

![image-20211031232446134](https://gitee.com/buptsg2019/picgo/raw/master/image-20211031232446134.png)

可以看到训练集和测试集的准确率相差很大，可能由于是 SVM 模型较为复杂的缘故，对这种较为简单的二分类问题不是很实用。不过，手写模型的准确率依然与 sklearn 库函数相差不大。

### Logistic 逻辑回归算法

#### 算法实现

逻辑回归算法在 `logistic.py` 中实现，封装为 `Logistic` 类，主要函数为训练函数：

- 训练函数采用 **梯度下降** 方法对回归模型进行训练，公式等说明在上次作业中说明过，不再过多赘述，实现方法如下：

```python
    def fit(self, X_train, y_train, alpha=0.01, n_iters=1e4):
        # 使用梯度下降法训练LR模型
        assert X_train.shape[0] == y_train.shape[0]  # 判断长度是否相等

        def J(theta, X_b, y):
            y_hat = self.sigmoid(X_b.dot(theta))
            try:
                return -np.sum(y * np.log(y_hat) + (1 - y) * np.log(1 - y_hat))
            except:
                return float('inf')

        def dJ(theta, X_b, y):
            # 求导后公式
            return X_b.T.dot(self.sigmoid(X_b.dot(theta)) - y) / len(y)

        def gradient_descent(X_b, y, initial_theta, alpha, n_iters=1e4, epsilon=1e-8):
            theta = initial_theta
            cur_iter = 0

            while cur_iter < n_iters:
                gradient = dJ(theta, X_b, y)
                last_theta = theta
                theta = theta - alpha * gradient
                if abs(J(theta, X_b, y) - J(last_theta, X_b, y)) < epsilon:
                    break
                cur_iter += 1
            return theta

        X_b = np.hstack([np.ones((len(X_train), 1)), X_train])
        initial_theta = np.zeros(X_b.shape[1])
        self._theta = gradient_descent(X_b, y_train, initial_theta, alpha, n_iters)

        # 截距
        self.intercept = self._theta[0]
        # x_i前的参数
        self.coef = self._theta[1:]

        return self
```

#### 结果测试

sklearn 库函数与手写函数对比结果如下：

![image-20211031233948163](https://gitee.com/buptsg2019/picgo/raw/master/image-20211031233948163.png)

可以看到模型准确率均在 90% 以上，与 sklearn 库函数相差不大。

### 其他算法

由于决策树算法和朴素贝叶斯算法适合离散值属性分类，连续值属性不太好实现，故由于时间原因暂时没有实现，现阐述一下几种实现连续值属性分类的思路：

- 将连续值通过数据预处理中的方法离散化，但缺点是数据集的数据分布可能不适合直接离散化，实现的分类效果差
- 通过网格搜索等方式，多次将数据进行离散化，找到分类效果最好的离散化形式
- 朴素贝叶斯可以通过计算高斯概率密度函数的方式实现，实现方法如下：

```python
#计算高斯概率密度函数
def CalcuGaussProb(self,x,mean,stdev):
    exponent = np.exp(-(np.power(x-mean,2))/(2*np.power(stdev,2)))
    GaussProb = (1/(np.sqrt(2*np.pi)*stdev))*exponent
    return GaussProb
```

计算概率时则将高斯概率密度相乘：

```python
#计算连续数据所属类的概率
def CalcuClassProbCon(self,arr,cx_mean,cx_std):
    cx_probabilities=1
    for i in range(len(cx_mean)):
        cx_probabilities *= self.CalcuGaussProb(arr[i],cx_mean[i],cx_std[i])
    return cx_probabilities
```

sklearn 测试结果如下：

![image-20211031235645085](https://gitee.com/buptsg2019/picgo/raw/master/image-20211031235645085.png)

![image-20211031235708298](https://gitee.com/buptsg2019/picgo/raw/master/image-20211031235708298.png)

