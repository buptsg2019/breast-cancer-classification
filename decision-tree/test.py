from sklearn import datasets, model_selection
from sklearn import tree

# 加载数据集
cancer = datasets.load_breast_cancer()
x = cancer.data  # 样本
y = cancer.target  # 类别

# 划分数据集
x_train, x_test, y_train, y_test = model_selection.train_test_split(x, y, test_size=0.2)

tree = tree.DecisionTreeClassifier(criterion='entropy')
tree.fit(x_train, y_train)

print("决策树模型训练集的准确率：%.3f" % tree.score(x_train, y_train))
print("决策树模型测试集的准确率：%.3f" % tree.score(x_test, y_test))
